declare module 'atob' {
    function atob(value: string): string;
    export = atob;
}
