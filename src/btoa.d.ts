declare module 'btoa' {
    function btoa(value: string): string;
    export = btoa;
}
