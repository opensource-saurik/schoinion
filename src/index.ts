/* Schoinion - Scatter-Gather Buffer Routines
 * Copyright (C) 2017  Jay Freeman (saurik)
*/

/* GNU Affero General Public License, Version 3 {{{ */
/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
/* }}} */

import * as assert from 'assert';
import atob = require('atob');
import btoa = require('btoa');
import * as te from 'text-encoding';

const encoder = new te.TextEncoder();
const decoder = new te.TextDecoder();

export function utf8encode(data: string): Uint8Array {
    return encoder.encode(data);
}

export function utf8decode(data: Uint8Array): string {
    return decoder.decode(data);
}

export function asc2encode(data: string): Uint8Array {
    const buffer = new ArrayBuffer(data.length);
    const array = new Uint8Array(buffer);
    for (let i = 0; i !== data.length; ++i)
        array[i] = data.charCodeAt(i);
    return array;
}

export function asc2decode(data: Uint8Array): string {
    return String.fromCharCode.apply(null, data);
}

export function js1encode(data: any): Uint8Array {
    return utf8encode(JSON.stringify(data));
}

export function js1decode(data: Uint8Array): any {
    return JSON.parse(utf8decode(data));
}

export function b64encode(data: Uint8Array): string {
    return btoa(asc2decode(data)).replace(/\+/g, "-").replace(/\//g, "_").replace(/=/g, "");
}

export function b64decode(data: string): Uint8Array {
    return asc2encode(atob(data.replace(/-/g, "+").replace(/_/g, "/")));
}


export function ie3encode(data: number): Uint8Array {
    const array = new Float64Array(1);
    array[0] = data;
    return new Uint8Array(array.buffer);
}

export function ie3decode(data: Uint8Array): number {
    assert.ok(data.length === 8);
    const array = new Float64Array(data.buffer, data.byteOffset, 1);
    return array[0];
}


export function join(parts: Uint8Array[]): Uint8Array {
    let total = 0;
    parts.forEach((part) => total += part.length);
    const joined = new Uint8Array(total);

    let offset = 0;
    parts.forEach((part) => {
        if (part.constructor !== Uint8Array)
            part = new Uint8Array(part.buffer, part.byteOffset, part.byteLength);
        joined.set(part, offset);
        offset += part.length;
    });

    return joined;
}

export function part(data: Uint8Array, sizes: number[]): Uint8Array[] {
    const parted = new Array<Uint8Array>();
    let offset = 0;
    sizes.forEach((size) => {
        if (size === 0)
            size = data.length - offset;
        const end = offset + size;
        assert.ok(data.length >= end);
        parted.push(data.subarray(offset, end));
        offset = end;
    });
    assert.ok(data.length === offset);
    return parted;
}

export function concat<T>(data: T[][]): T[] {
    return Array.prototype.concat.apply([], data);
}


export interface Kind {
    readonly BYTES_PER_ELEMENT: number;
    new(length: number): ArrayLike<number> & ArrayBufferView;
    new(array: ArrayLike<number>): ArrayLike<number> & ArrayBufferView;
    new(buffer: ArrayBufferLike, offset?: number, length?: number): ArrayLike<number> & ArrayBufferView;
}

export function num(kind: Kind, data: Uint8Array): number {
    assert.ok(data.length === kind.BYTES_PER_ELEMENT);
    return new kind(data.buffer, data.byteOffset, 1)[0];
}

export function buf(kind: Kind, value: number): Uint8Array {
    return new Uint8Array(new kind([value]).buffer);
}


export function xor(lhs: Uint8Array, rhs: Uint8Array): Uint8Array {
    const length = lhs.length;
    assert.ok(rhs.length === length);
    const value = new Uint8Array(length);
    for (let index = length; index-- !== 0; )
        value[index] = lhs[index] ^ rhs[index];
    return value;
}

export function add(lhs: Uint8Array, rhs: Uint8Array): Uint8Array {
    const length = lhs.length;
    assert.ok(rhs.length === length);
    const value = new Uint8Array(length);
    let carry = 0;
    for (let index = length; index-- !== 0; ) {
        const next = lhs[index] + (rhs[index] + carry);
        value[index] = next;
        carry = (next & 0x100) !== 0 ? 1 : 0;
    }
    return value;
}

export function sub(lhs: Uint8Array, rhs: Uint8Array): Uint8Array {
    const length = lhs.length;
    assert.ok(rhs.length === length);
    const value = new Uint8Array(length);
    let carry = 0;
    for (let index = length; index-- !== 0; ) {
        const next = lhs[index] - (rhs[index] + carry);
        value[index] = next;
        carry = (next & 0x100) !== 0 ? 1 : 0;
    }
    return value;
}

export function cmp(lhs: Uint8Array, rhs: Uint8Array): number {
    const length = lhs.length;
    assert.ok(rhs.length === length);
    for (let index = 0; index !== length; ++index)
        if (lhs[index] !== rhs[index])
            return lhs[index] < rhs[index] ? -1 : 1;
    return 0;
}

export function max(length: number): Uint8Array {
    return new Uint8Array(length).fill(0xff);
}
